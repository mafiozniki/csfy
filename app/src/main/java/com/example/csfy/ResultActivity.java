package com.example.csfy;

import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ResultActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.second_window);

        String fileName = getIntent().getStringExtra(MainActivity.FILE);
        TextView textView = findViewById(R.id.textView);
        textView.setText(Html.fromHtml(openFile(fileName)));
    }

    public String openFile(String fileName) {
        try {
            InputStream inputStream = getAssets().open(fileName);
            if (inputStream != null) {
                InputStreamReader tmp = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(tmp);
                String str;
                StringBuffer buffer = new StringBuffer();
                while ((str = reader.readLine()) != null) {
                    buffer.append(str);
                }
                inputStream.close();
                return buffer.toString();
            }

        } catch (Exception e) {
            return e.toString();
        }
        return "";
    }
}
